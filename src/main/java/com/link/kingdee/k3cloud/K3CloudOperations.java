package com.link.kingdee.k3cloud;

import java.util.List;

import com.link.kingdee.k3cloud.domain.BillQuery;
import com.link.kingdee.k3cloud.domain.BillSave;
import com.link.kingdee.k3cloud.domain.BillSubmit;

public interface K3CloudOperations {
	
	<T> T executeBillQuery(List<BillQuery> params, Class<T> type);
	
	<T> List<T> executeBillQuery(BillQuery billQuery, Class<T> type);
	
	<T> T executeBillSave(BillSave billSave, Class<T> type);

	<T, N> T executeBillSubmit(BillSubmit<N> billSubmit, Class<T> type);

}
