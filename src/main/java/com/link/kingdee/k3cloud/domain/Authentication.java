package com.link.kingdee.k3cloud.domain;

import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Authentication {
	
	/**
	 * 默认语言标识
	 */
	private static final int LCID = 2052;

	// 帐套id
	private String actId;

	private String username;

	private String password;

	// 语言标识
	private int lcid = LCID;
	
	public Authentication(String actId, String username, String password) {
		this(actId, username, password, LCID);
	}
	
	public List<?> toReqParamList() {
		return Arrays.asList(actId, username, password, lcid);
	}
	
}
