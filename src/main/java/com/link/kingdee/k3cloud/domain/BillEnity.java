package com.link.kingdee.k3cloud.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class BillEnity {
	
	protected transient String formId;
	
}
