package com.link.kingdee.k3cloud.service;

import com.link.kingdee.k3cloud.domain.BillQuery;
import com.link.kingdee.k3cloud.domain.RequestService;

public class BillQueryService extends CommonService<BillQuery> {

	@Override
	public RequestService getService() {
		return RequestService.SERVICE_EXECUTE_BILL_QUERY;
	}

}
