package com.link.kingdee.k3cloud.service;

import com.link.kingdee.k3cloud.domain.RequestService;

public class BillSubmitService extends CommonService<Object> {

    @Override
    public RequestService getService() {
        return RequestService.SERVICE_SUBMIT;
    }

}

